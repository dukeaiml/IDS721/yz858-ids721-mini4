use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use std::collections::HashMap;
use lazy_static::lazy_static;

lazy_static! {
    static ref FRUITS_INFO: HashMap<String, f64> = {
        let mut map = HashMap::new();
        map.insert("apple".to_string(), 3.0);
        map.insert("orange".to_string(), 2.0);
        map.insert("pear".to_string(), 1.5);
        map
    };
}

async fn greet(fruit: web::Path<String>) -> impl Responder {
    HttpResponse::Ok().body(format!("Hello, you asked for {}!\n
                                    Price of that is {}.", fruit, FRUITS_INFO.get(&*fruit).copied().unwrap_or(0.0)))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/{fruit}", web::get().to(greet))  // Adjusted route
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
