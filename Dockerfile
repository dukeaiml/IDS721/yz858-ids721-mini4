FROM rust:1.75 AS builder

# Set the working directory
WORKDIR /usr/src/yz858-ids721-mini4

# Change the user to root
USER root

# Copy everything
COPY . .

# Build the project
RUN cargo build --release

# Expose port 8080
EXPOSE 8080

# Define the default command to run
CMD cargo run

