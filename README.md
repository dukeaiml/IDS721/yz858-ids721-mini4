# yz858-ids721-mini4

This project holds a simple Rust web service to return price of a given fruit fruits.

## Requirements
- [x] Container functionality: containerize simple rust Actix web app (40%)
- [x] Dockerfile and build with docker image (40%)
- [x] Documentation (Dockerfile, screenshots of running container, process writeup) (20%)

## Steps
1. Create a brand new project
```
cargo new NAME
```

2. Add required dependencies into 'Cargo.toml'
- actix-web
- tokio

3. Run the web server locally for testing before containerization
```
cargo run
```

4. Create Dockerfile and build docker image
```
docker build -t IMAGE_NAME .
```

5. Run container
```
docker run -p 8080:8080 IMAGE_NAME
```


## Screenshots
1. Successful Docker image build
![Build](/screenshots/BuildSuccess.png)

2. Running container
![Container](/screenshots/RunningContainer.png)

3. Running services
![Service](/screenshots/ServiceRunning.png)

